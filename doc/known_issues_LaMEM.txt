This file will list known issues/bugs that need to be fixed at a later stage.

===================================================================================================
1) November 9, 2010  - PERIODIC BOUNDARY CONDITIONS DON'T WORK WITH GALERKIN GMG

SVN INFO:
MacBook-BorisKaus:LaMEM kausb$ svn info
Path: .
URL: svn+ssh://kausb@musashi.ethz.ch/var/svn/boriskaus/LaMEM/trunk
Repository Root: svn+ssh://kausb@musashi.ethz.ch/var/svn
Repository UUID: 26174531-3d5a-4d31-aba0-7a6658d032cb
Revision: 1197
Node Kind: directory
Schedule: normal
Last Changed Author: kausb
Last Changed Rev: 1197
Last Changed Date: 2010-11-01 13:33:33 +0100 (Mon, 01 Nov 2010)

REPRODUCE ERROR:
./LaMEM -restart 0 -vpt_element Q1P0 -BC.LeftBound 3 -BC.FrontBound 3

[0]PETSC ERROR: --------------------- Error Message ------------------------------------
[0]PETSC ERROR: Argument out of range!
[0]PETSC ERROR: Negative index -54 at 0 location!
[0]PETSC ERROR: ------------------------------------------------------------------------
[0]PETSC ERROR: Petsc Release Version 3.1.0, Patch 5, Mon Sep 27 11:51:54 CDT 2010
[0]PETSC ERROR: See docs/changes/index.html for recent updates.
[0]PETSC ERROR: See docs/faq.html for hints about trouble shooting.
[0]PETSC ERROR: See docs/index.html for manual pages.
[0]PETSC ERROR: ------------------------------------------------------------------------
[0]PETSC ERROR: ./LaMEM on a darwin9.8 named geop-063.ethz.ch by kausb Tue Nov  9 11:59:32 2010
[0]PETSC ERROR: Libraries linked from /Users/kausb/Software/PETSC/petsc-3.1-p5/darwin9.8.0-c-debug/lib
[0]PETSC ERROR: Configure run at Thu Oct 14 14:43:21 2010
[0]PETSC ERROR: Configure options -with-cc=gcc --with-fc=gfortran --download-blacs=1 --download-scalapack=1 --download-f-blas-lapack=1 -download-mpich=1 --download-mumps=1 --with-shared=0 --download-ml=1 --download-parmetis=1 --download-hypre=1
[0]PETSC ERROR: ------------------------------------------------------------------------
[0]PETSC ERROR: VecScatterCheckIndices_Private() line 26 in src/vec/vec/utils/vscat.c
[0]PETSC ERROR: VecScatterCreate() line 941 in src/vec/vec/utils/vscat.c
[0]PETSC ERROR: MeshDefineCoarseGeometry() line 352 in "unknowndirectory/"Mesh.c
[0]PETSC ERROR: main() line 346 in "unknowndirectory/"LaMEM.c
application called MPI_Abort(MPI_COMM_WORLD, 63) - process 0[unset]: aborting job:
application called MPI_Abort(MPI_COMM_WORLD, 63) - process 0


SYMPTOM:
The error does not occur if we do not involve GMG, e.g. with
./LaMEM -restart 0 -vpt_element Q1P0 -BC.LeftBound 3 -SolverType 3 -UzawaSolver 3 -levels 1 -BC.FrontBound 3


POSSIBLE FIX:
This is probably fixed in Petsc 3.2 so we can test this once more as soon as the DA set coordinate stuff has been replaced again with the one in the one of Petsc 3.2

===================================================================================================


2) February 8 2011 - FDSTAG DOES NOT WORK WITH VARIABLE dx/dy/dz


previously, we had to create the DA for FDSTAG with a stencilwidth of 2, in order for the code to run on 
multiple cores.
Turns out that this was caused by a routine that computes dx/dy/dz. 
I added a workaround which causes LaMEM to function only in case of constant dx/dy/dz if combined with FDSTAG. 
I also added a rudimentary check to see if this is indeed fulfilled. If not, the code will stop and give an error message. 

It is likely that we do not need this and a fix is straightforward, but bugfixing this requires a more substantial checking of the FDSTAG stencilscode,
which I leave for the future.  



===================================================================================================


3) February 12 2011 - Boundary Conditions

i added internal boundary conditions. for using them, you have to pay special attention.
the place where you apply them depends on grid size, geometry, etc.
when assigning them be careful to change nodes/elements at three different places:
- ComputeStiffnessMatrixes_FEM
- SetBC_ElementStiffnessMatrix_Symmetric
- SetBoundaryConditionsRHS

this is due to a somewhat mixed up way of assigning the BCs.
in ComputeStiffnessMatrixes_FEM and SetBoundaryCondiationsRHS BCs are assigned via global node numbers.
however, in SetBC_ElementStiffnessMatrix_Symmetric via local element numbers.

in future it would be nice to make this more consistent.
