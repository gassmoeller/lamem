================================================================================

Marker initialization parameter "msetup" can assume the following values:

 parallel,    // read coordinates, phase and temperature from files in parallel
 redundant,   // read phase and temperature from file redundantly (uniform coordinates)
 diapir,      // diapir setup
 block,       // falling block
 subduction,  // subduction setup with air
 folding,     // multilayer folding setup (Zagros)
 detachment,  // 1-layer over detachment (Grasemann & Schmalholz 2012)
 slab,        // slab detachment (Thieulot et al. 2014)
 spheres      // multiple falling spheres

Loading markers from files in parallel and redundantly uses the parameters:

- LoadInitialParticlesDirectory (default InitialParticles)
- ParticleFilename (default ParticlesInput)

The file name pattern for parallel files is ./LoadInitialParticlesDirectory/ParticleFilename.ProcessID.dat
The file name pattern for redundant file is ./LoadInitialParticlesDirectory/ParticleFilename.dat

================================================================================

Particles can be saved to disk for later use with different resolution or for debugging.
Only parallel writing mode is supported.
The saved markers are supposed to be loaded with parallel load option (msetup parallel)

To activate particle saving use this parameter:
SaveParticles

The directory for saving particles is controlled by the parameter:
SaveInitialParticlesDirectory (default InitialParticles)

The file pattern is:
./SaveInitialParticlesDirectory/ParticleFilename.dat


================================================================================
